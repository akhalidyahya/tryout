<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateResultsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('results', function (Blueprint $table) {
            $table->id();
            $table->unsignedInteger('user_id');
            $table->unsignedInteger('tryout_id');
            $table->dateTime('tpa_start_time')->nullable();
            $table->dateTime('tbi_start_time')->nullable();
            $table->dateTime('tpa_end_time')->nullable();
            $table->dateTime('tbi_end_time')->nullable();
            $table->integer('tpa_score',5)->nullable();
            $table->integer('tbi_score',5)->nullable();
            $table->integer('total_score')->nullable();
            $table->timestamps();
            $table->index('tryout_id');
            $table->index('user_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('results');
    }
}
