<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateQuestionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('questions', function (Blueprint $table) {
            $table->id();
            $table->int('question_number',11);
            $table->unsignedInteger('tryout_id');
            $table->string('question_type',15);
            $table->text('question_text')->nullable();
            $table->string('question_image')->nullable();
            $table->string('answer_a')->nullable();
            $table->string('answer_b')->nullable();
            $table->string('answer_c')->nullable();
            $table->string('answer_d')->nullable();
            $table->string('answer_e')->nullable();
            $table->string('right_answer',1);
            $table->timestamps();
            $table->index('tryout_id');
            $table->index('question_type');
            $table->index('question_number');
            $table->index(['question_number','tryout_id','question_type']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('questions');
    }
}
