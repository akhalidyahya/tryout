<?php

namespace Database\Seeders;

use App\Models\Tryout;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class InitSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name'              => 'admin',
            'email'             => 'admin@gmail.com',
            'password'          => bcrypt('1234'),
            'role'              => 'ADMIN',
            'ttl'               => '-',
            'jurusan'           => '-',
            'jenis_kelamin'     => 'MALE',
            'nomor_pendaftaran' => '-',
            'status_pembayaran' => 'PAID',
            'token'             => Str::random(50)
        ]);
        User::create([
            'name'              => 'user',
            'email'             => 'user@gmail.com',
            'password'          => bcrypt('1234'),
            'role'              => 'USER',
            'ttl'               => '-',
            'jurusan'           => 'DIII AKUNTANSI',
            'jenis_kelamin'     => 'MALE',
            'status_pembayaran' => 'UNPAID',
            'nomor_pendaftaran' => date('YmdHis'),
            'token'             => Str::random(50)
        ]);
        User::create([
            'name'              => 'user 2',
            'email'             => 'user2@gmail.com',
            'password'          => bcrypt('1234'),
            'role'              => 'USER',
            'ttl'               => '-',
            'jurusan'           => 'DIII PAJAK',
            'jenis_kelamin'     => 'FEMALE',
            'status_pembayaran' => 'UNPAID',
            'nomor_pendaftaran' => date('YmdHis'),
            'token'             => Str::random(50)
        ]);
        Tryout::create([
            'tryout_name'       => 'TO #1',
            'start_time'        => Carbon::now(),
            'end_time'          => Carbon::now()->addDay(),
        ]);
        Tryout::create([
            'tryout_name'       => 'TO #2',
            'start_time'        => Carbon::now()->addDays(2),
            'end_time'          => Carbon::now()->addDays(3),
        ]);
    }
}
