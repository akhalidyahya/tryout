<?php

namespace App\Exports;

use App\Models\User;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

use App\Utilities\Constants;

class DataUserExcel implements FromCollection, WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */

    public function headings():array{
        return [
            'No',
            'Name',
            'Email',
            'Jurusan',
            'Nomor pendaftaran',
            'Status bayar',
            'Role'

        ];
    }

    public function collection()
    {
        // $data = [
        //     'name'=>$request->name,
        //     'email'=>$request->email,
        //     'password'=> bcrypt($request->password),
        //     'ttl'=>$request->ttl,
        //     'jurusan'=>$request->jurusan,
        //     'jenis_kelamin'=>$request->jenis_kelamin,
        //     'nomor_pendaftaran'=>date('YmdHis'),
        //     'token'=>Str::random(50),
        //     'role'=>Constants::ROLE_USER,
        //     'status_pembayaran'=>Constants::REGISTRATION_STATUS_UNPAID,
        // ];
        
        $user = User::select('id', 'name', 'email', 'jurusan', 'nomor_pendaftaran', 'status_pembayaran', 'role')->get();
        return $user;

    }

}
