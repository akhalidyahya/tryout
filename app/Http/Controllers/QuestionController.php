<?php

namespace App\Http\Controllers;

use App\Models\Question;
use App\Models\Tryout;
use Illuminate\Http\Request;
use DataTables;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Response;

class QuestionController extends Controller
{
    public function index($id)
    {
        $tryout        = Tryout::find($id);
        if(empty($tryout)) {
            return back();
        }

        $data['tryout']     = $tryout;
        $data['sidebar']    = 'tryouts';
        return view('pages/questions',$data);
    }

    public function saveQuestion(Request $request)
    {
        if(empty($request->id)){
            $question = new Question();
        } else {
            $question = Question::find($request->id);
        }

        if($request->hasFile('question_image')) {
            if(!empty($question->question_image)) {
                unlink($question->question_image);
            }
            $file = $request->file('question_image');
            $newFileName = Carbon::now()->format('d-m-Y-H-i-s').'-'.$file->getClientOriginalName();
            $request->file('question_image')->storeAs('questions', $newFileName,'public');
            $question->question_image   = 'public/storage/questions/'.$newFileName;
        }
        $question->question_number  = $request->question_number;
        $question->tryout_id        = $request->tryout_id;
        $question->question_type    = $request->question_type;
        $question->question_text    = $request->question_text;
        $question->answer_a         = $request->answer_a;
        $question->answer_b         = $request->answer_b;
        $question->answer_c         = $request->answer_c;
        $question->answer_d         = $request->answer_d;
        $question->answer_e         = $request->answer_e;
        $question->right_answer     = $request->right_answer;

        $response = [
            'success'   => false,
            'message'   => 'Data Soal gagal disimpan'
        ];
        if($question->save()){
            $response['success'] = true;
            $response['message'] = 'Data Soal berhasil disimpan';
        }
        return response()->json($response);
    }

    public function dataTable(Request $request, $type)
    {
        $tryout_id  = $request->tryout_id;
        $datas      = Question::where('tryout_id',$tryout_id)
                                ->where('question_type',$type)
                                ->orderBy('question_number','ASC')->get();
                                // ->orderBy(DB::raw('CAST(question_number AS unsigned)'),'ASC')->get();

        return DataTables::of($datas)
            ->editColumn('question_text',function($datas){
                return substr(strip_tags($datas->question_text),0,50).'...';
            })
            ->editColumn('question_image',function($datas){
                if(!empty($datas->question_image)) {
                    return '<img class="" src="'.url($datas->question_image).'" height="50px" />';
                }
                else {
                    return 'No image';
                }
            })
            ->addColumn('action', function ($datas) {
                return '<div class="btn-group">
                            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Manage <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu">
                                <li><a onclick="editData('.$datas->id.')" href="javascript:void(0);"><i class="fa fa-edit"></i> Edit</a></li>
                                <li><a onclick="deleteData('.$datas->id.')" href="javascript:void(0);"><i class="fa fa-trash"></i> Delete</a></li>
                            </ul>
                        </div>';
            })
            ->escapeColumns([])->make(true);
    }
    
    public function getQuestionById(Request $request)
    {
        $id = $request->id;
        return response()->json(Question::find($id));
    }

    public function deleteQuestion(Request $request)
    {
        $question = Question::find($request->id);
        if(!empty($question->question_image)){
            unlink($question->question_image);
        }
        Question::destroy($request->id);

        if ($question) {
            return Response::json(array('success' => true, 'message' => 'Data berhasil dihapus'));
        } else {
            return Response::json(array('success' => false, 'message' => 'Data Gagal dihapus, coba lagi'));
        }
    }
}
