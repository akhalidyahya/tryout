<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Concerns\FromCollection;
use App\Exports\DataUserExcel;
use Maatwebsite\Excel\Facades\Excel;
use DB;

class ExcelController extends Controller
{
    public function export()
    {
        return Excel::download(new DataUserExcel, 'Data User Excel.xlsx');
    }
}

