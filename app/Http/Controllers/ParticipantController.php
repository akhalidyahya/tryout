<?php

namespace App\Http\Controllers;

use App\Mail\PaymentConfirmed;
use Response;
use DataTables;
use App\Models\Participant;
use App\Models\User;
use Illuminate\Support\Str;
use App\Utilities\Constants;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class ParticipantController extends Controller
{
    public function index()
    {
        $data['sidebar']    = 'participants';

        return view('pages/participants', $data);
    }

    public function getParticipantById(Request $request)
    {
        $id = $request->id;
        $participant             = Participant::find($id);
        return Participant::find($id);
    }

    public function saveParticipant(Request $request)
    {
        $response = [
            'success'   => false,
            'message'   => 'Data Peserta gagal disimpan'
        ];

        if(empty($request->id)){
            $participant = new Participant();
            $participant->nomor_pendaftaran = date('YmdHis');
            $participant->token = Str::random(50);
            $participant->role =Constants::ROLE_USER;
        } else {
            $participant = Participant::find($request->id);
        }

        $participant->name    = $request->name;
        $participant->email    = $request->email;
        $participant->ttl    = $request->ttl;
        $participant->jurusan    = $request->jurusan;
        $participant->jenis_kelamin    = $request->jenis_kelamin;
        
        if (!empty($request->password)) {
            $participant->password    = bcrypt($request->password);
        }
        
        if($participant->save()){
            $response['success'] = true;
            $response['message'] = 'Data Participant berhasil disimpan';
        }
        return response()->json($response);
    }

    public function deleteParticipant(Request $request)
    {

        $participant = Participant::destroy($request->id);

        if ($participant) {
            return Response::json(array('success' => true, 'message' => 'Data berhasil dihapus'));
        } else {
            return Response::json(array('success' => false, 'message' => 'Data Gagal dihapus, coba lagi'));
        }
    }

    public function changeStatus(Request $request,$user_id)
    {
        $response = [
            'success'   => false,
            'message'   => 'Status gagal diubah'
        ];
        $participant = User::find($user_id);

        $participant->status_pembayaran =   $request->status_pembayaran;

        if($request->status_pembayaran == Constants::REGISTRATION_STATUS_PAID) {
            try {
                Mail::to($participant->email)->send( new PaymentConfirmed($participant));
            } 
            catch(\Exception $e) {
                
            }
        }

        if ($participant->save()) {
            return Response::json(array('success' => true, 'message' => 'Status berhasil diupdate'));
        } else {
            return Response::json($response);
        }
    }

    public function dataTable()
    {
        $datas = Participant::where('role',Constants::ROLE_USER)->orderBy('id','desc')->get();

        return DataTables::of($datas)
            ->editColumn('bukti_pembayaran', function ($datas) {
                if(!empty($datas->bukti_pembayaran)) {
                    return '<img class="" src="'.url($datas->bukti_pembayaran).'" height="50px" />';
                }
                else {
                    return 'No image';
                }
            })
            ->editColumn('status_pembayaran', function ($datas) {
                return Constants::REGISTRATION_STATUS_LISTS[$datas->status_pembayaran];
            })
            ->addColumn('action', function ($datas) {
                return '<div class="btn-group">
                                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        Manage <span class="caret"></span>
                                    </button>
                                    <ul class="dropdown-menu">
                                        <li><a onclick="changeStatus('.$datas->id.' , \' '. Constants::REGISTRATION_STATUS_PAID .' \')" href="javascript:void(0);"><i class="fa fa-check"></i> Accept</a></li>
                                        <li><a onclick="changeStatus('.$datas->id.',\' '.Constants::REGISTRATION_STATUS_REJECT.'\')" href="javascript:void(0);"><i class="fa fa-times"></i> Reject</a></li>
                                        <li role="separator" class="divider"></li>
                                        <li><a onclick="editData('.$datas->id.')" href="javascript:void(0);"><i class="fa fa-edit"></i> Edit</a></li>
                                        <li><a onclick="deleteData('.$datas->id.')" href="javascript:void(0);"><i class="fa fa-trash"></i> Delete</a></li>
                                    </ul>
                                </div>';
            })
            ->escapeColumns([])->make(true);
    }

}
