<?php

namespace App\Http\Controllers;

use App\Mail\AccountActivation;
use App\Models\User;
use Illuminate\Support\Str;
use App\Utilities\Constants;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Validator;

class AuthController extends Controller
{
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function loginIndex()
    {
        return view('login');
    }
    public function loginProcess(Request $request)
    {
        $request->validate([
            'email' => 'required',
            'password' => 'required'
        ]);

        $credentials = $request->except(['_token']);

        $user = User::where('email',$request->email)->first();

        if (Auth::attempt($credentials)) {
            $request->session()->regenerate();

            //email verified only
            if(Auth::user()->email_verified_at == NULL && Auth::user()->role == Constants::ROLE_USER) {
                Auth::logout();
                session()->flash('message', 'Email not verified');
                return redirect()->back();
            }
            
            return redirect()->route('dashboard.index');

        }else{
            session()->flash('message', 'Invalid credentials');
            return redirect()->back();
        }
    }

    public function saveData(Request $request)
    {
        $rules = [
            'email' => 'required|email|unique:users'
        ];
 
        $messages = [
            'email.unique'  => 'Email sudah terdaftar.',
        ];
 
        $validator = Validator::make($request->all(), $rules, $messages);
         
        if($validator->fails()){
            return redirect()->back()->withErrors($validator)->withInput( $request->all() );
        }

        $data = [
            'name'=>$request->name,
            'email'=>$request->email,
            'password'=> bcrypt($request->password),
            'ttl'=>$request->ttl,
            'jurusan'=>$request->jurusan,
            'jenis_kelamin'=>$request->jenis_kelamin,
            'nomor_pendaftaran'=>date('YmdHis'),
            'token'=>Str::random(50),
            'role'=>Constants::ROLE_USER,
            'status_pembayaran'=>Constants::REGISTRATION_STATUS_UNPAID,
        ];
        
        $user = User::create($data);
        
        try {
            Mail::to($data['email'])->send( new AccountActivation($user));
        } 
        catch(\Exception $e) {
            return redirect()->route('login.index')->with('message','Something went wrong when sending mail. Please check if your mail is an active mail or contact admin');
        }

        return redirect()->route('login.index')->with('success','Pendaftaran Berhasil! Silahkan cek email untuk aktivasi akun');
    }
    
    public function registerIndex()
    {
        return view('register');
    }

    public function sendActivation($id)
    {
        $user = User::find($id);
        Mail::to($user->email)->send( new AccountActivation($user));
    }

    public function activation($user_id,$token)
    {
        $user = User::where('id',$user_id)->where('token',$token)->first();
        if(empty($user)) {
            return redirect()->route('register.index');
        }
        if(!empty($user->email_verified_at)) {
            return redirect()->route('login.index')->with('message','Akun pernah diaktivasi, silahkan login dengan email');
        }
        $user->email_verified_at = Carbon::now();
        
        if($user->save()) {
            return redirect()->route('login.index')->with('success','Aktivasi akun berhasil! Silahkan login dengan email');
        } else {
            return redirect()->route('login.index')->with('success','Something went wrong!');
        }
    }

    public function logout()
    {
        \Auth::logout();

        return redirect()->route('login.index');
    }
}
