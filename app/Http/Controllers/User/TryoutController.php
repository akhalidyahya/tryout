<?php

namespace App\Http\Controllers\User;

use App\Helper\GeneralHelper;
use App\Http\Controllers\Controller;
use App\Models\Answer;
use App\Models\Question;
use App\Models\Result;
use App\Models\Tryout;
use App\Utilities\Constants;
use Illuminate\Http\Request;
use DataTables;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\DB;
use PHPUnit\TextUI\XmlConfiguration\Constant;

class TryoutController extends Controller
{
    public function index()
    {
        $data['sidebar']    =   'tryouts';

        return view('user_pages/tryouts', $data);
    }

    public function tryoutInfo($tryout_id)
    {
        $tryout = Tryout::where('id',$tryout_id)->first();
        
        $result = Result::where('user_id',\Auth::id())->where('tryout_id',$tryout_id)->first();

        if(empty($result)) {
            $result = Result::create([
                'user_id'           => Auth::id(),
                'tryout_id'         => $tryout_id,
                'tpa_start_time'    => NULL,
                'tbi_start_time'    => NULL,
                'tpa_score'         => NULL,
                'tbi_score'         => NULL
            ]);
        }

        if(GeneralHelper::isTryOutTimeOut(Constants::QUESTION_TYPE_TPA,$result->tpa_start_time)){
            self::calculateScore($tryout_id,Constants::QUESTION_TYPE_TPA);
            $result = Result::where('user_id',\Auth::id())->where('tryout_id',$tryout_id)->first();
        }
        if(GeneralHelper::isTryOutTimeOut(Constants::QUESTION_TYPE_TBI,$result->tbi_start_time)){
            self::calculateScore($tryout_id,Constants::QUESTION_TYPE_TBI);
            $result = Result::where('user_id',\Auth::id())->where('tryout_id',$tryout_id)->first();
        }

        $tryout->result     = $result;

        $tpa_first_question =   Question::where('tryout_id',$tryout_id)
                                            ->where('question_type',Constants::QUESTION_TYPE_TPA)
                                            ->orderBy('question_number','asc')
                                            ->first()->question_number;
        $tbi_first_question =   Question::where('tryout_id',$tryout_id)
                                            ->where('question_type',Constants::QUESTION_TYPE_TBI)
                                            ->orderBy('question_number','asc')
                                            ->first()->question_number;

        $data['sidebar']    =   'tryouts';
        $data['tryout']     =   $tryout;
        $data['isGoingOn']  =   GeneralHelper::isTryOutOnGoing($tryout->start_time,$tryout->end_time);
        $data['tpa_first_question'] =   $tpa_first_question;
        $data['tbi_first_question'] =   $tbi_first_question;
        $data['tpa_question_count'] =   Question::where('tryout_id',$tryout_id)
                                                ->where('question_type',Constants::QUESTION_TYPE_TPA)
                                                ->count();
        $data['tbi_question_count'] =   Question::where('tryout_id',$tryout_id)
                                                ->where('question_type',Constants::QUESTION_TYPE_TBI)
                                                ->count();

        return view('user_pages/tryouts-info', $data);
    }

    public function tryout($tryout_id,$type,$question_number)
    {
        
        $tryout         =   Tryout::find($tryout_id);
        //cek apakah tryout start time belum mulai
        if(!GeneralHelper::isTryOutOnGoingOrDone($tryout->start_time,$tryout->end_time)) {
            return back();
        }
        $question       =   Question::where('question_number',$question_number)
                                            ->where('tryout_id',$tryout_id)
                                            ->where('question_type',$type)
                                            ->first();
        $prev_question  =   Question::where('question_number','<',$question_number)
                                            ->where('tryout_id',$tryout_id)
                                            ->where('question_type',$type)
                                            ->orderBy('question_number','desc')
                                            ->first();
        $next_question  =   Question::where('question_number','>',$question_number)
                                            ->where('tryout_id',$tryout_id)
                                            ->where('question_type',$type)
                                            ->orderBy('question_number','asc')
                                            ->first();
        $first_question =   Question::where('tryout_id',$tryout_id)
                                            ->where('question_type',$type)
                                            ->orderBy('question_number','asc')
                                            ->first()->question_number;
        $last_question  =   Question::where('tryout_id',$tryout_id)
                                            ->where('question_type',$type)
                                            ->orderBy('question_number','desc')
                                            ->first()->question_number;
        $result         =   Result::where('tryout_id',$tryout_id)->where('user_id',\Auth::id())->first();
        $user_answer    =   Answer::where('tryout_id',$tryout_id)
                                            ->where('user_id',\Auth::id())
                                            ->where('question_id',$question->id)
                                            ->first();
        $question->answer = $user_answer;

        
        if($type == Constants::QUESTION_TYPE_TPA) {
            if(!empty($result->tpa_score) && GeneralHelper::isTryOutOnGoing($tryout->start_time,$tryout->end_time)) {
                return back();
            }   
            $result_score       =   $result->tpa_score;
            $remaining_time     =   Carbon::parse($result->tpa_start_time)->addMinutes(Constants::TPA_TIME)->diffInSeconds(Carbon::now());
            if(GeneralHelper::isTryOutTimeOut(Constants::QUESTION_TYPE_TPA,$result->tpa_start_time)){
                $remaining_time =   0;
            }
        } else {
            if(!empty($result->tbi_score) && GeneralHelper::isTryOutOnGoing($tryout->start_time,$tryout->end_time)) {
                return back();
            }
            $result_score       =   $result->tbi_score;
            $remaining_time     =   Carbon::parse($result->tbi_start_time)->addMinutes(Constants::TBI_TIME)->diffInSeconds(Carbon::now());
            if(GeneralHelper::isTryOutTimeOut(Constants::QUESTION_TYPE_TBI,$result->tbi_start_time)){
                $remaining_time =   0;
            }
        }

        if(GeneralHelper::isTryoutDone($tryout->end_time)) {
            $remaining_time = 0;
        }

        $data['type']               =   $type;
        $data['sidebar']            =   'tryouts';
        $data['tryout']             =   $tryout;
        $data['result_score']       =   $result_score;
        $data['question']           =   $question;
        $data['next_question']      =   $next_question;
        $data['prev_question']      =   $prev_question;
        $data['isFirstQuestion']    =   ($question->question_number == $first_question) ? true : false;
        $data['isLastQuestion']     =   ($question->question_number == $last_question) ? true : false;
        $data['remaining_time']     =   $remaining_time;
        $data['all_questions']      =   Question::select('question_number')->where('tryout_id',$tryout_id)->where('question_type',$type)->orderBy('question_number','asc')->get();
        $data['isTryoutEnd']        =   GeneralHelper::isTryoutDone($tryout->end_time);

        return view('user_pages/tryout-questions',$data);
    }

    public function startTryoutTimer($tryout_id,$type)
    {
        $result = Result::where('user_id',\Auth::id())->where('tryout_id',$tryout_id)->first();
        
        if(empty($result)) {
            return response()->json(['success'=>false]);
        }
        if($type == Constants::QUESTION_TYPE_TPA && empty($result->tpa_start_time)) {
            $result->tpa_start_time = Carbon::now();
        } 
        elseif($type == Constants::QUESTION_TYPE_TBI && empty($result->tbi_start_time)) {
            $result->tbi_start_time = Carbon::now();
        }
        
        if($result->save()) {
            return response()->json(['success'=>true]);
        } else {
            return response()->json(['success'=>false]);
        }
    }

    public function calculateScore($tryout_id,$type, $user_id = NULL)
    {
        if(!empty($user_id)) {
            $result = Result::where('user_id',$user_id)->where('tryout_id',$tryout_id)->first();
        } else {
            $result = Result::where('user_id',\Auth::id())->where('tryout_id',$tryout_id)->first();
        }

        if(empty($result)) {
            return response()->json(['success'=>false]);
        }

        if(!empty($user_id)) {
            $questions  =   Question::with(['answers'=>function($q)use($user_id){
                                    $q->where('user_id',$user_id);
                                }])
                                ->where('tryout_id',$tryout_id)
                                ->where('question_type',$type)
                                ->get();
        } else {
            $questions  =   Question::with(['answers'=>function($q){
                $q->where('user_id',\Auth::id());
            }])
            ->where('tryout_id',$tryout_id)
            ->where('question_type',$type)
            ->get();
        }

        $right_answers  = 0;
        $wrong_answers  = 0;
        $not_answered   = 0;

        if(!empty($questions)) {
            foreach($questions as $question) {
                if(count($question->answers) < 1) {
                    $not_answered++;
                    continue;
                }
                if($question->right_answer == $question->answers[0]->user_answer) {
                    $right_answers++;
                }else {
                    $wrong_answers++;
                }
            }
        }
        
        $right_answer_score = Constants::TPA_RIGHT_POINT;
        $wrong_answer_score = Constants::TPA_WRONG_POINT;
        $null_answer_score  = Constants::TPA_NULL_POINT;
        if($type == Constants::QUESTION_TYPE_TBI) {
            $right_answer_score = Constants::TBI_RIGHT_POINT;
            $wrong_answer_score = Constants::TBI_WRONG_POINT;
            $null_answer_score  = Constants::TBI_NULL_POINT;
        }

        $score  =   ($right_answers * $right_answer_score) + ($wrong_answers * $wrong_answer_score) + ($not_answered * $null_answer_score);

        //save ke kolom tpa atau tbi
        if($type == Constants::QUESTION_TYPE_TPA) {
            $result->tpa_score  = $score;
        } else {
            $result->tbi_score  = $score;
        }
        if($result->save()) {
            if(!empty($user_id)) {
                self::calculateTotalScore($tryout_id,$user_id);    
            } else {
                self::calculateTotalScore($tryout_id);
            }
            return response()->json(['success'=>true,'score'=>$score]);
        } else {
            return response()->json(['success'=>false]);
        }
    }

    public function calculateScoreManual($tryout_id,$type, $user_id)
    {
        self::calculateScore($tryout_id,$type, $user_id);
    }

    public function calculateTotalScore($tryout_id,$user_id = NULL)
    {
        if(!empty($user_id)) {
            $result = Result::where('user_id',$user_id)->where('tryout_id',$tryout_id)->first();
        } else {
            $result = Result::where('user_id',\Auth::id())->where('tryout_id',$tryout_id)->first();
        }
        if(empty($result)) {
            return response()->json(['success'=>false]);
        }
        $tpa = 0;
        if(is_numeric($result->tpa_score)) {
            $tpa = $result->tpa_score;
        }
        $tbi = 0;
        if(is_numeric($result->tbi_score)) {
            $tbi = $result->tbi_score;
        }
        $result->total_score = $tpa + $tbi;
        $result->save();
    }

    public function saveAnswer(Request $request, $tryout_id,$question_id)
    {
        $tryout = Tryout::find($tryout_id);
        if(GeneralHelper::isTryoutDone($tryout->end_time)) {
            return response()->json(['success'=>false,'message'=>'Tryout is done']);
        }

        Answer::where('user_id',\Auth::id())
            ->where('tryout_id',$tryout_id)
            ->where('question_id',$question_id)->delete();

        $answer                 = new Answer();
        $answer->user_id        = \Auth::id();
        $answer->tryout_id      = $tryout_id;
        $answer->question_id    = $question_id;
        $answer->user_answer    = $request->user_answer;
        if($answer->save()) {
            return response()->json(['success'=>true]);
        } else {
            return response()->json(['success'=>false]);
        }
    }

    public function tryoutRank($tryout_id)
    {
        $data['sidebar']    = 'tryouts';
        $data['tryout']     = Tryout::find($tryout_id);

        return view('user_pages/ranks', $data);
    }

    public function dataTable()
    {
        $datas = Tryout::all();

        return DataTables::of($datas)
            ->editColumn('start_time',function($datas){
                return Carbon::parse($datas->start_time)->format('D, d F Y - H:i');
            })
            ->editColumn('end_time',function($datas){
                return Carbon::parse($datas->end_time)->format('D, d F Y - H:i');
            })
            ->addColumn('action', function ($datas) {
                if(GeneralHelper::isTryOutComingSoon($datas->start_time,$datas->end_time)) {
                    return 'Coming soon';
                }
                $button = '<div class="btn-group">
                            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Opsi <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu">';
                if(GeneralHelper::isTryOutOnGoing($datas->start_time,$datas->end_time)) {
                    $button .= '<li><a href="'.route('user.tryouts.info',['tryout_id'=>$datas->id]).'"><i class="fa fa-clock-o"></i> Mulai TryOut</a></li>';
                    $button .= '<li><a href="'.route('user.tryouts.rank',['tryout_id'=>$datas->id]).'" href="javascript:void(0);"><i class="fa fa-users"></i> Rank</a></li>';
                } 
                else {
                    $button .= '<li><a href="'.route('user.tryouts.info',['tryout_id'=>$datas->id]).'" href="javascript:void(0);"><i class="fa fa-search"></i> Hasil</a></li>';
                    $button .= '<li><a href="'.route('user.tryouts.rank',['tryout_id'=>$datas->id]).'" href="javascript:void(0);"><i class="fa fa-users"></i> Rank</a></li>';
                }

                $result = Result::where('user_id',\Auth::id())->where('tryout_id',$datas->id)->first();

                if(!empty($result)) {
                    if( (GeneralHelper::isTryOutTimeOut(Constants::QUESTION_TYPE_TPA,$result->tpa_start_time)
                        &&
                        GeneralHelper::isTryOutTimeOut(Constants::QUESTION_TYPE_TBI,$result->tbi_start_time) )
                        ||
                        !GeneralHelper::isTryOutOnGoing($datas->start_time,$datas->end_time)
                        ) 
                    {
                        $button .= '<li><a href="'.route('tryouts.files',['tryout_id'=>$datas->id]).'"><i class="fa fa-download"></i> Download Files</a></li>';
                    }
                }

                $button .= '</ul></div>';
                return $button;
            })
            ->escapeColumns([])->make(true);
    }

    public function rankDataTable(Request $request,$tryout_id)
    {
        $datas = Result::with('user')
                    ->whereHas('user',function($q) {
                        $q->whereNull('deleted_at');
                    })
                    ->where('tryout_id',$tryout_id);
        if($request->jurusan) {
            $datas->whereHas('user',function($q) use($request) {
                $q->where('jurusan',$request->jurusan);
            });
        }
        $datas->orderBy('total_score','desc')
            ->orderBy('tpa_score','desc')
            ->orderBy('tbi_score','desc')
            ->get();
        
        return DataTables::of($datas)
            ->addColumn('name',function($datas){
                $name = @$datas->user->name;
                if(@$datas->user->id == \Auth::id()) {
                    return '<b style="color:maroon">'.$name.'</b>';
                }
                $name = @GeneralHelper::censorName($name);
                return $name;
            })
            ->addColumn('registration_number',function($datas){
                $num = @$datas->user->nomor_pendaftaran;
                if(@$datas->user->id == \Auth::id()) {
                    return '<b style="color:maroon">'.$num.'</b>';
                }
                $num = @GeneralHelper::censorNumber($num);
                return $num;
            })
            ->addColumn('jurusan',function($datas){
                if(@$datas->user->id == \Auth::id()) {
                    return '<b style="color:maroon">'.@Constants::JURUSAN_LISTS[@$datas->user->jurusan].'</b>';
                }
                return @Constants::JURUSAN_LISTS[@$datas->user->jurusan];
            })
            ->editColumn('tpa_score',function($datas){
                $tpa = 0;
                if(is_numeric($datas->tpa_score)) {
                    $tpa = $datas->tpa_score;
                }
                // if($datas->user->id == \Auth::id()) {
                //     return '<b style="color:maroon">'.$tpa.'</b>';
                // }
                return $tpa;
            })
            ->editColumn('tbi_score',function($datas){
                $tbi = 0;
                if(is_numeric($datas->tbi_score)) {
                    $tbi = $datas->tbi_score;
                }
                // if($datas->user->id == \Auth::id()) {
                //     return '<b style="color:maroon">'.$tbi.'</b>';
                // }
                return $tbi;
            })
            ->addColumn('total_score',function($datas){
                $total = 0;
                if(is_numeric($datas->total_score)) {
                    $total = $datas->total_score;
                }
                // if($datas->user->id == \Auth::id()) {
                //     return '<b style="color:maroon">'.$total.'</b>';
                // }
                return $total;
                // $tpa = 0;
                // $tbi = 0;
                // if(is_numeric($datas->tpa_score)) {
                //     $tpa = $datas->tpa_score;
                // }
                // if(is_numeric($datas->tbi_score)) {
                //     $tbi = $datas->tbi_score;
                // }
                // if($datas->user->id == \Auth::id()) {
                //     return '<b>'.$tpa + $tbi.'</b>';  
                // }
                // return $tpa + $tbi;
            })
            ->escapeColumns([])->make(true);
    }
}
