<?php

namespace App\Http\Controllers\User;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Utilities\Constants;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Image;


class PembayaranController extends Controller
{
    public function index()
    {
        return view('user_pages\pembayaran');
    }

    public function uploadBukti(Request $request)
    {
        $user = User::find(\Auth::id());

        if($request->hasFile('bukti_pembayaran')) {
            if(!empty($user->bukti_pembayaran)) {
                unlink($user->bukti_pembayaran);
            }
            $file = $request->file('bukti_pembayaran');
            $newFileName = time().'.'.$file->getClientOriginalExtension();
            $destinationPath = public_path('/storage/registration');
            $img = Image::make($file->getRealPath());
            $img->resize(250,500, function ($constraint) {
                $constraint->aspectRatio();
            });
            // ->save($destinationPath.'/'.$newFileName);
            Storage::disk('public')->put("registration/".$newFileName, (string) $img->encode());

            $user->bukti_pembayaran     = 'public/storage/registration/'.$newFileName;
            $user->status_pembayaran    = Constants::REGISTRATION_STATUS_ON_CONFIRMATION;
            if($user->save()) {
                return response()->json(['success'=>true,'message'=>'Bukti pembayaran berhasil di upload dan akan dikonfirmasi oleh admin']);
            } else {
                return response()->json(['success'=>false,'message'=>'Bukti pembayaran gagal di upload!']);
            }
        }
        
        return response()->json(['success'=>false,'message'=>'Bukti pembayaran gagal di upload!']);
    }
}
