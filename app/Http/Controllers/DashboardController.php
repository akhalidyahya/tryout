<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class DashboardController extends Controller
{
    public function index()
    {
        $data['sidebar']    =   'dashboard';
        return view('pages/dashboard',$data);
    }
}
