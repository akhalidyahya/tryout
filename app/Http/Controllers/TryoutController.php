<?php

namespace App\Http\Controllers;

use App\Helper\GeneralHelper;
use App\Models\File;
use App\Models\Result;
use Illuminate\Http\Request;
use DataTables;
use App\Utilities\Constants;
use Response;
use Illuminate\Support\Carbon;

use App\Models\Tryout;

class TryoutController extends Controller
{
    public function index()
    {
        $data['sidebar']    =   'tryouts';

        return view('pages/tryouts', $data);
    }

    public function getTryoutById(Request $request)
    {
        $id                 = $request->id;
        $tryout             = Tryout::find($id);
        $tryout->start_time = Carbon::parse($tryout->start_time)->format('D d F Y - H:i');
        $tryout->end_time   = Carbon::parse($tryout->end_time)->format('D d F Y - H:i');
        return $tryout;
    }

    public function saveTryout(Request $request)
    {
        $response = [
            'success'   => false,
            'message'   => 'Data Tryout gagal disimpan'
        ];

        if(empty($request->id)){
            $tryout = new Tryout();
        } else {
            $tryout = Tryout::find($request->id);
        }

        $tryout->tryout_name    = $request->tryout_name;
        $tryout->start_time     = Carbon::createFromFormat('D d F Y - H:i',$request->start_time);
        $tryout->end_time       = Carbon::createFromFormat('D d F Y - H:i',$request->end_time);
        
        if($tryout->save()){
            $response['success'] = true;
            $response['message'] = 'Data Tryout berhasil disimpan';
        }
        return response()->json($response);
    }

    public function deleteTryout(Request $request)
    {
        $tryout = Tryout::destroy($request->id);

        if ($tryout) {
            return Response::json(array('success' => true, 'message' => 'Data berhasil dihapus'));
        } else {
            return Response::json(array('success' => false, 'message' => 'Data Gagal dihapus, coba lagi'));
        }
    }

    public function dataTable()
    {
        $datas = Tryout::all();

        return DataTables::of($datas)
            ->editColumn('start_time',function($datas){
                return Carbon::parse($datas->start_time)->format('D, d F Y - H:i');
            })
            ->editColumn('end_time',function($datas){
                return Carbon::parse($datas->end_time)->format('D, d F Y - H:i');
            })
            ->addColumn('action', function ($datas) {
                return '<div class="btn-group">
                            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Manage <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu">
                                <li><a onclick="questionData('.$datas->id.')" href="javascript:void(0);"><i class="fa fa-list-ol"></i> Pertanyaan</a></li>
                                <li><a onclick="editData('.$datas->id.')" href="javascript:void(0);"><i class="fa fa-edit"></i> Edit</a></li>
                                <li><a href="'.route('tryouts.rank',['tryout_id'=>$datas->id]).'"><i class="fa fa-users"></i> Ranking</a></li>
                                <li><a href="'.route('tryouts.files',['tryout_id'=>$datas->id]).'"><i class="fa fa-upload"></i> Upload Files</a></li>
                                <li role="separator" class="divider"></li>
                                <li><a onclick="deleteData('.$datas->id.')" href="javascript:void(0);"><i class="fa fa-trash"></i> Delete</a></li>
                            </ul>
                        </div>';
            })
            ->escapeColumns([])->make(true);
    }

    public function tryoutRank($tryout_id)
    {
        $data['sidebar']    = 'tryouts';
        $data['tryout']     = Tryout::find($tryout_id);

        return view('pages/ranks', $data);
    }

    public function rankDataTable(Request $request, $tryout_id)
    {
        $datas = Result::with('user')
                    ->whereHas('user',function($q) {
                        $q->whereNull('deleted_at');
                    })
                    ->where('tryout_id',$tryout_id);
        if($request->jurusan) {
            $datas->whereHas('user',function($q) use($request) {
                $q->where('jurusan',$request->jurusan);
            });
        }
        $datas->orderBy('total_score','desc')
            ->orderBy('tpa_score','desc')
            ->orderBy('tbi_score','desc')
            ->get();
        
        return DataTables::of($datas)
            ->addColumn('name',function($datas){
                $name = $datas->user->name;
                return $name;
            })
            ->addColumn('registration_number',function($datas){
                $num = $datas->user->nomor_pendaftaran;
                return $num;
            })
            ->addColumn('jurusan',function($datas){
                return @Constants::JURUSAN_LISTS[$datas->user->jurusan];
            })
            ->editColumn('tpa_score',function($datas){
                $tpa = 0;
                if(is_numeric($datas->tpa_score)) {
                    $tpa = $datas->tpa_score;
                }
                return $tpa;
            })
            ->editColumn('tbi_score',function($datas){
                $tbi = 0;
                if(is_numeric($datas->tbi_score)) {
                    $tbi = $datas->tbi_score;
                }
                return $tbi;
            })
            ->addColumn('total_score',function($datas){
                $total = 0;
                if(is_numeric($datas->total_score)) {
                    $total = $datas->total_score;
                }
                return $total;
            })
            ->escapeColumns([])->make(true);
    }

    public function tryoutFiles($tryout_id) {
        $data['sidebar']    = 'tryouts';
        $data['tryout']     = Tryout::with('files')->where('id',$tryout_id)->first();

        if(\Auth::user()->role == Constants::ROLE_USER) {

            $result = Result::where('user_id',\Auth::id())->where('tryout_id',$tryout_id)->first();

            if(!( GeneralHelper::isTryOutTimeOut(Constants::QUESTION_TYPE_TPA,$result->tpa_start_time)
                &&
                GeneralHelper::isTryOutTimeOut(Constants::QUESTION_TYPE_TBI,$result->tbi_start_time) )
                &&
                GeneralHelper::isTryOutOnGoing($data['tryout']->start_time,$data['tryout']->end_time)
                ) 
            {
                return back();
            }
        }

        return view('pages/files', $data);
    }

    public function uploadFile(Request $request) {
        $file = new File();

        if($request->hasFile('upload_file')) {
            $file->file_name    = $request->file_name;
            $file->tryout_id    = $request->tryout_id;

            $document           = $request->file('upload_file');
            $newFileName        = time().'.'.$document->getClientOriginalExtension();
            $request->file('upload_file')->storeAs('files', $newFileName,'public');

            $file->file_path    = 'public/storage/files/'.$newFileName;
            $file->save();
        }
        
        $response = [
            'success'   => false,
            'message'   => 'File gagal disimpan'
        ];

        if($file->save()){
            $response['success'] = true;
            $response['message'] = 'File berhasil disimpan';
        }
        return response()->json($response);
    }

    public function deleteFile($id) 
    {
        $file = File::find($id);
        if(!empty($file->file_path)){
            unlink($file->file_path);
        }
        File::destroy($id);

        if ($file) {
            return Response::json(array('success' => true, 'message' => 'File berhasil dihapus'));
        } else {
            return Response::json(array('success' => false, 'message' => 'File Gagal dihapus, coba lagi'));
        }
    }
}
