<?php 
namespace App\Utilities;

class Constants {
    const ROLE_ADMIN    = 'ADMIN';
    const ROLE_USER     = 'USER';

    const QUESTION_TYPE_TPA = 'TPA';
    const QUESTION_TYPE_TBI = 'TBI';

    const REGISTRATION_STATUS_PAID              = 'PAID';
    const REGISTRATION_STATUS_UNPAID            = 'UNPAID';
    const REGISTRATION_STATUS_ON_CONFIRMATION   = 'ON CONFIRMATION';
    const REGISTRATION_STATUS_REJECT            = 'REJECT';

    const REGISTRATION_STATUS_LISTS = [
        self::REGISTRATION_STATUS_PAID              => 'Pembayaran Selesai',
        self::REGISTRATION_STATUS_UNPAID            => 'Menunggu Pembayaran',
        self::REGISTRATION_STATUS_ON_CONFIRMATION   => 'Sedang Dikonfirmasi',
        self::REGISTRATION_STATUS_REJECT            => 'Ditolak/Bukti Tidak Valid. Silahkan Coba Upload Lagi',
    ];

    const TPA_TIME  = 100;
    const TBI_TIME  = 50;

    const TPA_RIGHT_POINT   = 4;
    const TPA_WRONG_POINT   = -1;
    const TPA_NULL_POINT    = 0;
    const TBI_RIGHT_POINT   = 4;
    const TBI_WRONG_POINT   = -1;
    const TBI_NULL_POINT    = 0;

    const D3_AKUNTANSI  =   'DIII AKUNTANSI';
    const D3_PAJAK      =   'DIII PAJAK';
    const D3_BC         =   'DIII KEPABEANAN DAN CUKAI';
    const JURUSAN_LISTS =   [
        self::D3_AKUNTANSI  =>  'DIII Akuntansi',
        self::D3_PAJAK      =>  'DIII Pajak',
        self::D3_BC         =>  'DIII Kepabeanan dan Cukai',
    ];

    const TRYOUT_STATUS_COMING_SOON = 'COMING SOON';
    const TRYOUT_STATUS_ON_GOING    = 'ON GOING';
    const TRYOUT_STATUS_DONE        = 'DONE';
    const TRYOUT_STATUS_LISTS       = [
        self::TRYOUT_STATUS_COMING_SOON => 'Akan berlangsung',
        self::TRYOUT_STATUS_ON_GOING    => 'Sedang berlangsung',
        self::TRYOUT_STATUS_DONE        => 'Selesai',
    ];
}

?>