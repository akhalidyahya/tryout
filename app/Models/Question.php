<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    use HasFactory;
    protected $table = 'questions';

    protected $fillable = [
        'question_number',
        'tryout_id',
        'question_type',
        'question_text',
        'question_image',
        'answer_a',
        'answer_b',
        'answer_c',
        'answer_d',
        'answer_e',
        'right_answer',
    ];

    protected $casts = [
        'question_number' => 'integer',
    ];

    public function tryout()
    {
        return $this->belongsTo(Tryout::class, 'tryout_id', 'id');
    }

    public function answer()
    {
        return $this->hasOne(Answer::class, 'question_id', 'id');
    }

    public function answers()
    {
        return $this->hasMany(Answer::class, 'question_id', 'id');
    }
}
