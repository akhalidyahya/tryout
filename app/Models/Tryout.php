<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Tryout extends Model
{
    use HasFactory,SoftDeletes;
    protected $table = 'tryouts';
    protected $fillable = [
        'tryout_name',
        'start_time',
        'end_time',
    ];

    public function questions()
    {
        return $this->hasMany(Question::class, 'tryout_id', 'id');
    }
   
    public function results()
    {
        return $this->hasMany(Result::class,'tryout_id','id');
    }

    public function files()
    {
        return $this->hasMany(File::class,'tryout_id','id');
    }
}
