<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Result extends Model
{
    use HasFactory;
    protected $table = 'results';
    protected $fillable = [
        'user_id',
        'tryout_id',
        'tpa_start_time',
        'tbi_start_time',
        'tpa_end_time',
        'tbi_end_time',
        'score',
    ];

    public function tryout()
    {
        return $this->belongsTo(Tryout::class,'tryout_id','id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
    
}
