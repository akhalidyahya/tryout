﻿<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>Register | Mulai Belajar Yok</title>
    <!-- Favicon-->
    <link rel="icon" href="{{asset('admin/favicon.ico')}}" type="image/x-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <!-- Bootstrap Core Css -->
    <link href="{{asset('admin/plugins/bootstrap/css/bootstrap.css')}}" rel="stylesheet">

    <!-- Waves Effect Css -->
    <link href="{{asset('admin/plugins/node-waves/waves.css')}}" rel="stylesheet" />

    <!-- Animation Css -->
    <link href="{{asset('admin/plugins/animate-css/animate.css')}}" rel="stylesheet" />

    <!-- Custom Css -->
    <link href="{{asset('admin/css/style.css')}}" rel="stylesheet">
</head>

<body class="login-page">
    <div class="login-box">
        <div class="logo">
            <a href="{{url('/')}}">Mulai Belajar <b>Yok</b></a>
            <small>Aplikasi Try out Online</small>
        </div>
        @if ($errors->has('email'))
            <div class="alert alert-danger alert-block">
                <button type="button" class="close" data-dismiss="alert">×</button>	
                <strong>{{ $errors->first('email') }}</strong>
            </div>
        @endif
        <div class="card">
            <div class="body">
                <form id="sign_in" method="POST" action="{{route('register.save')}}">
                    {{csrf_field()}}
                    <div class="msg">Register!</div>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">person</i>
                        </span>
                        <div class="form-line">
                            <input type="text" class="form-control" name="name" placeholder="Nama Contoh : Mimin" required autofocus autocomplete="off" value="{{ old('name') }}">
                        </div>
                    </div>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">email</i>
                        </span>
                        <div class="form-line">
                            <input type="email" class="form-control" name="email" placeholder="Email Contoh : name@email.com" required autocomplete="off">
                        </div>
                    </div>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">lock</i>
                        </span>
                        <div class="form-line">
                            <input type="password" class="form-control" id="password" name="password" placeholder="Password Min 4 karakter" required>
                        </div>
                    </div>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">today</i>
                        </span>
                        <div class="form-line">
                            <input type="text" class="form-control" name="ttl" placeholder="TTL Contoh : Depok, 31 Februari 1999" required autocomplete="off" value="{{ old('ttl') }}">
                        </div>
                    </div>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">poll</i>
                        </span>
                        <div class="col-sm-10">
                            <select class="form-control show-tick" name="jurusan" required>
                                <option value="">--- Pilih Target Jurusan ---</option>
                                @foreach(\App\Utilities\Constants::JURUSAN_LISTS as $key => $jurusan)
                                <option value="{{$key}}">{{$jurusan}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">wc</i>
                        </span>
                        <div class="col-sm-10">
                            <select class="form-control show-tick" name="jenis_kelamin" required>
                                <option value="">--- Pilih Jenis Kelamin ---</option>
                                <option value="MALE">Laki Laki</option>
                                <option value="FEMALE">Perempuan</option>
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="text-center">
                                <button class="btn btn-block bg-pink waves-effect" type="submit">Register</button>
                            </div>
                        </div>
                    </div>
                    Sudah memiliki akun? <a href={{ 'login' }}>Login</a>
                </form>
            </div>
        </div>
    </div>

    <!-- Jquery Core Js -->
    <script src="{{asset('admin/plugins/jquery/jquery.min.js')}}"></script>

    <!-- Bootstrap Core Js -->
    <script src="{{asset('admin/plugins/bootstrap/js/bootstrap.js')}}"></script>

    <!-- Waves Effect Plugin Js -->
    <script src="{{asset('admin/plugins/node-waves/waves.js')}}"></script>

    <!-- Validation Plugin Js -->
    <script src="{{asset('admin/plugins/jquery-validation/jquery.validate.js')}}"></script>

    <!-- Custom Js -->
    <script src="{{asset('admin/js/admin.js')}}"></script>
    <script src="{{asset('admin/js/pages/examples/sign-in.js')}}"></script>
</body>

</html>