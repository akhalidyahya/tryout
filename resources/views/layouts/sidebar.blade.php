<!-- User Info -->
<div class="user-info">
    <div class="image">
        <img src="{{asset('admin/images/user.png')}}" width="48" height="48" alt="User" />
    </div>
    <div class="info-container">
        <div class="name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">{{@\Auth::user()->name}}</div>
        <div class="email">{{@\Auth::user()->email}}</div>
        <div class="btn-group user-helper-dropdown">
            <i class="material-icons" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">keyboard_arrow_down</i>
            <ul class="dropdown-menu pull-right">
                <!-- <li><a href="javascript:void(0);"><i class="material-icons">person</i>Profile</a></li> -->
                <li role="separator" class="divider"></li>
                <li><a href="{{route('logout')}}" onclick="event.preventDefault(); document.getElementById('frm-logout').submit();"><i class="material-icons">input</i>Sign Out</a></li>
                <form id="frm-logout" action="{{ route('logout') }}" method="POST" style="display: none;">
                    {{ csrf_field() }}
                </form>
            </ul>
        </div>
    </div>
</div>
<!-- #User Info -->
<!-- Menu -->
<div class="menu">
    <ul class="list">
        <li class="header">MAIN NAVIGATION</li>
        <li class="{{@$sidebar == 'dashboard' ? 'active': ''}}">
            <a href="{{route('dashboard.index')}}">
                <i class="material-icons">dashboard</i>
                <span>Dasbor</span>
            </a>
        </li>
        @if(\App\Helper\GeneralHelper::isAdmin())
        <li class="{{@$sidebar == 'tryouts' ? 'active': ''}}">
            <a href="{{route('tryouts.index')}}">
                <i class="material-icons">assignment</i>
                <span>Tryouts</span>
            </a>
        </li>
        <li class="{{@$sidebar == 'participants' ? 'active': ''}}">
            <a href="{{route('participants.index')}}">
                <i class="material-icons">perm_identity</i>
                <span>Peserta</span>
            </a>
        </li>
        @endif

        @if(!\App\Helper\GeneralHelper::isAdmin() && \Auth::user()->status_pembayaran == \App\Utilities\Constants::REGISTRATION_STATUS_PAID)
        <li class="{{@$sidebar == 'tryouts' ? 'active': ''}}">
            <a href="{{route('user.tryouts.index')}}">
                <i class="material-icons">assignment</i>
                <span>Tryouts</span>
            </a>
        </li>
        @endif
    </ul>
</div>
<!-- #Menu -->
<!-- Footer -->
<div class="legal">
    <div class="copyright">
        &copy; <a href="javascript:void(0);">Mulai Belajar Yok</a>.
    </div>
    <div class="version">
        <b>Version: </b> 1
    </div>
</div>
<!-- #Footer -->