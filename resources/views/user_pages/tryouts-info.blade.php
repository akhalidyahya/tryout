@extends('layouts.base')
@section('title','Mulai Tryout')
@push('customCSS')
<style>
    ol li {
        margin: 10px 0px 10px 0px;
    }
</style>
@endpush
@section('content')
<div class="container-fluid">
    <div class="block-header">
        @if($isGoingOn)
        <h2>Mulai Tryout</h2>
        @else
        <h2>Jawaban Tryout</h2>
        @endif
    </div>
    <div class="row clearfix">
        <div class="col-lg-12">
            <div class="card">
                <div class="header">
                    <h2><a href="{{route('user.tryouts.index')}}"><i class="fa fa-chevron-left"></i></a> {{$tryout->tryout_name}}</h2>
                </div>
                <div class="body">
                    <div class="row">
                        @if($isGoingOn)
                        <div class="col-md-12">
                            <h4>Petunjuk Pengisian</h4>
                            <ol>
                                <li>
                                    Terdapat dua jenis paket soal
                                    <ol type="a">
                                        <li>Tes Potensi Akademik</li>
                                        <li>Tes Bahasa Inggris</li>
                                    </ol>
                                </li>
                                <li>
                                    Tes Potensi Akademik (TPA) terdiri dari 120 Soal dengan batas waktu mengerjakan (100 Menit)
                                </li>
                                <li>
                                    Tes Bahasa Inggris (TBI) terdiri dari 60 Soal dengan batas waktu mengerjakan (50 Menit)
                                </li>
                                <li>
                                    Peserta bebas untuk memilih mengerjakan TPA atau TBI terlebih dahulu
                                </li>
                                <li>
                                    Setelah peserta men klik tombol mulai maka waktu mengerjakan akan otomatis berjalan
                                </li>
                                <li>
                                    Apabila browser ditutup atau peserta malakukan Logout waktu mengerjakan akan terus berjalan
                                </li>
                                <li>
                                    Untuk mengakhiri TO peserta <b>WAJIB</b> men klik tombol selesai yang ada di soal terakhir
                                </li>
                                <li>
                                    Setelah mengakhiri TO peserta dapat melihat ranking dan nilainya
                                </li>
                                <li>
                                    Setelah peserta menyelesaikan semua paket soal peserta dapat <b>MENDOWNLOAD FILE SOAL DAN PEMBAHASAN</b> di menu “action” <i class="fa fa-arrow-right"></i> ”download”
                                </li>
                            </ol>
                        </div>
                        @else
                        <div class="col-md-12">
                            <h4>Hasil Tryout</h4>
                            <p>
                                <i class="fa fa-circle" style="color:#20df80"></i> Warna hijau = jawaban yg benar. <br>
                                <i class="fa fa-circle" style="color:red"></i> Warna merah = jawaban yg anda dipilih (bila salah) <br>
                                *) Apabila jawaban yg anda pilih benar, maka yang muncul warna hijau juga.
                            </p>
                        </div>
                        @endif
                        <div class="clearfix"></div>
                        <div class="col-md-6">
                            <strong>TPA</strong>
                            <p>
                                Waktu: {{\App\Utilities\Constants::TPA_TIME}} Menit, <br>
                                Jumlah Soal: {{ $tpa_question_count }}, <br>
                                Waktu mulai: {{ !empty($tryout->result->tpa_start_time) ? @\Carbon\Carbon::parse(@$tryout->result->tpa_start_time)->format('d F Y H:i') : '-' }} <br>
                                Skor: {{@$tryout->result->tpa_score}}
                            </p>
                            @if($isGoingOn)
                            <button class="btn btn-primary" onclick="startTryout('TPA')">Mulai TPA</button>
                            @else
                            <a class="btn btn-primary" href="{{route('user.tryouts.tryout',['tryout_id'=>$tryout->id,'type'=>\App\Utilities\Constants::QUESTION_TYPE_TPA,'question_number'=>$tpa_first_question])}}">Jawaban TPA</a>
                            @endif

                        </div>
                        <div class="col-md-6">
                            <strong>TBI</strong>
                            <p>
                                Waktu: {{\App\Utilities\Constants::TBI_TIME}} Menit, <br>
                                Jumlah Soal: {{ $tbi_question_count }}, <br>
                                Waktu mulai: {{ !empty($tryout->result->tbi_start_time) ? @\Carbon\Carbon::parse(@$tryout->result->tbi_start_time)->format('d F Y H:i') : '-' }} <br>
                                Skor: {{@$tryout->result->tbi_score}}
                            </p>
                            @if($isGoingOn)
                            <button class="btn btn-primary" onclick="startTryout('TBI')">Mulai TBI</button>
                            @else
                            <a class="btn btn-primary" href="{{route('user.tryouts.tryout',['tryout_id'=>$tryout->id,'type'=>\App\Utilities\Constants::QUESTION_TYPE_TBI,'question_number'=>$tbi_first_question])}}">Jawaban TBI</a>
                            @endif
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@push('customJS')
<script>
    function startTryout(type) {
        swal({
            title: "Anda yakin?",
            text: `Dengan ini ${type} akan dimulai`,
            icon: "warning",
            buttons: true,
            dangerMode: true,
        }).then((done) => {
            if (done) {
                let url = "{{route('user.tryouts.startTimer',['tryout_id'=>$tryout->id,'type'=>':type'])}}";
                url = url.replace(':type', type);
                $.ajax({
                    url: url,
                    type: 'GET',
                    success: function(data){
                        if(data.success){
                            var urlRedirect = "{{route('user.tryouts.tryout',['tryout_id'=>$tryout->id,'type'=>':type','question_number'=>':question_number'])}}";
                            urlRedirect = urlRedirect.replace(':type', type);
                            if(type == 'TPA') {
                                urlRedirect = urlRedirect.replace(':question_number',"{{$tpa_first_question}}");
                            } else {
                                urlRedirect = urlRedirect.replace(':question_number',"{{$tbi_first_question}}");
                            }
                            window.location.href = urlRedirect;
                        } else {
                            swal('Gagal','Silahkan coba lagi atau hubungi CP','warning');
                        }
                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                        swal({
                            title: 'System Error',
                            text: errorThrown,
                            icon: 'error',
                            timer: '3000'
                        });
                    }
                });
            }
        });
    }
</script>
@endpush
@endsection