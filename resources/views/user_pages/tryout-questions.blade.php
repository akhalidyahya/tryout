@extends('layouts.base')
@section('title','Tryout')
@push('customCSS')
<!-- Font Awesome -->
<link href="{{asset('admin/css/radio.css')}}" rel="stylesheet" />
@endpush
@section('content')
<meta name="csrf-token" content="{{ csrf_token() }}">
<div class="container-fluid">
    <div class="block-header">
        <h2>{{@$tryout->tryout_name}}</h2>
    </div>
    <div class="row clearfix">
        <div class="col-lg-12">
            <div class="card">
                <div class="header">
                    <div class="row">
                        <div class="col-md-12">
                            <h2><a href="{{route('user.tryouts.info',['tryout_id'=>$tryout->id])}}"><i class="fa fa-chevron-left"></i></a> {{@$type}}</h2>
                            <div class="right">
                                @if(!$isTryoutEnd || $result_score == NULL)
                                Waktu anda tersisa <strong id="countdown">00:00</strong>
                                @else
                                Skor {{$type}}: {{@$result_score}}
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                <div class="body">
                    <div class="row">
                        <div class="col-md-12">
                            <h4>No. {{$question->question_number}}</h4>
                            <p>
                                @if(!empty($question->question_image))
                                <img src="{{url($question->question_image)}}" alt="" class="img-responsive">
                                @endif
                            </p>
                            <div style="line-height: 1.7;">
                                {!! @$question->question_text !!}
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-md-12">
                            <section class="radio-section">
                                <div>
                                    <input type="radio" id="control_01" name="answer" value="A" 
                                        @if(!empty($question->answer)) 
                                            @if(@$question->answer->user_answer == 'A') checked @endif 
                                        @endif
                                        >
                                    <label for="control_01"
                                        @if( $remaining_time == 0 && $question->right_answer == 'A')
                                        class="true-answer"
                                        @elseif( $remaining_time == 0 && @$question->answer->user_answer == 'A')
                                        class="wrong-answer"
                                        @endif
                                        >
                                        <h2>A</h2>
                                        <p>{{@$question->answer_a}}</p>
                                    </label>
                                </div>
                                <div>
                                    <input type="radio" id="control_02" name="answer" value="B" 
                                        @if(!empty($question->answer)) 
                                            @if(@$question->answer->user_answer == 'B') checked @endif 
                                        @endif
                                        >
                                    <label for="control_02"
                                        @if( $remaining_time == 0 && $question->right_answer == 'B')
                                        class="true-answer"
                                        @elseif( $remaining_time == 0 && @$question->answer->user_answer == 'B')
                                        class="wrong-answer"
                                        @endif
                                        >
                                        <h2>B</h2>
                                        <p>{{@$question->answer_b}}</p>
                                    </label>
                                </div>
                                <div>
                                    <input type="radio" id="control_03" name="answer" value="C" 
                                        @if(!empty($question->answer)) 
                                            @if(@$question->answer->user_answer == 'C') checked @endif 
                                        @endif
                                        >
                                    <label for="control_03"
                                        @if( $remaining_time == 0 && $question->right_answer == 'C')
                                        class="true-answer"
                                        @elseif( $remaining_time == 0 && @$question->answer->user_answer == 'C')
                                        class="wrong-answer"
                                        @endif
                                        >
                                        <h2>C</h2>
                                        <p>{{@$question->answer_c}}</p>
                                    </label>
                                </div>
                                <div>
                                    <input type="radio" id="control_04" name="answer" value="D" 
                                        @if(!empty($question->answer)) 
                                            @if(@$question->answer->user_answer == 'D') checked @endif 
                                        @endif
                                        >
                                    <label for="control_04"
                                        @if( $remaining_time == 0 && $question->right_answer == 'D')
                                        class="true-answer"
                                        @elseif( $remaining_time == 0 && @$question->answer->user_answer == 'D')
                                        class="wrong-answer"
                                        @endif
                                        >
                                        <h2>D</h2>
                                        <p>{{@$question->answer_d}}</p>
                                    </label>
                                </div>
                                <div>
                                    <input type="radio" id="control_05" name="answer" value="E" 
                                        @if(!empty($question->answer)) 
                                            @if(@$question->answer->user_answer == 'E') checked @endif 
                                        @endif
                                        >
                                    <label for="control_05"
                                        @if( $remaining_time == 0 && $question->right_answer == 'E')
                                        class="true-answer"
                                        @elseif( $remaining_time == 0 && @$question->answer->user_answer == 'E')
                                        class="wrong-answer"
                                        @endif
                                        >
                                        <h2>E</h2>
                                        <p>{{@$question->answer_e}}</p>
                                    </label>
                                </div>
                            </section>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-12">
            <div class="card">
                <div class="body">
                    <div class="row">
                        <div class="col-sm-6" style="margin-bottom: 0;">
                            <div class="row text-left">
                                <div class="col-xs-4 p-r-0" style="margin-bottom: 0;">
                                    <select class="form-control show-tick" name="question_number" style="width: 100%;">
                                        <option value="">--- Pilih Soal ---</option>
                                        @foreach($all_questions as $question_option)
                                        <option value="{{$question_option->question_number}}" @if($question_option->question_number == $question->question_number) selected @endif >Soal Nomor {{$question_option->question_number}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-xs-8 p-l-5" style="margin-bottom: 0;">
                                    <button onclick="selectPage()" class="btn btn-primary waves-effect"><i class="material-icons">search</i></button>
                                    <span class="m-r-20"></span>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6" style="margin-bottom: 0;">
                            <div class="text-right">
                                <button onclick="goTo('{{@$prev_question->question_number}}')" class="btn btn-primary waves-effect" @if($isFirstQuestion) disabled @endif><i class="material-icons">keyboard_arrow_left</i></button>
                                <button onclick="goTo('{{@$next_question->question_number}}')" class="btn btn-primary waves-effect" @if($isLastQuestion) disabled @endif> <i class="material-icons">keyboard_arrow_right</i></button>
                                @if((!$isTryoutEnd || $result_score == NULL) && $isLastQuestion)
                                <button id="endBtn" onclick="endTryout()" class="btn btn-success waves-effect"><i class="material-icons">check</i><span>Selesai</span> </button>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@push('customJS')
<script>
    function selectPage() {
        goTo($('[name="question_number"]').val());
    }

    function endTryout() {
        let type = "{{$type}}";
        swal({
            title: "Anda yakin?",
            text: `Dengan ini ${type} akan selesai`,
            icon: "warning",
            buttons: true,
            dangerMode: true,
        }).then((done) => {
            if (done) {
                let url = "{{route('user.tryouts.calculateScore',['tryout_id'=>$tryout->id,'type'=>':type'])}}";
                url = url.replace(':type', type);
                $.ajax({
                    url: url,
                    type: 'GET',
                    success: function(data){
                        // console.log(data);
                        if(data.success){
                            var urlRedirect = "{{route('user.tryouts.info',['tryout_id'=>$tryout->id])}}";
                            window.location.href = urlRedirect;
                        } else {
                            swal('Gagal','Silahkan coba lagi atau hubungi CP','warning');
                        }
                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                        swal({
                            title: 'System Error',
                            text: errorThrown,
                            icon: 'error',
                            timer: '3000'
                        });
                    }
                });
            }
        });
    }

    function goTo(num) {
        let url = "{{route('user.tryouts.tryout',['tryout_id'=>$tryout->id,'type'=>$type,'question_number'=>':questionNumber'])}}";
        url = url.replace(':questionNumber',num);
        window.location.href=url;
    }

    $('input[type=radio][name=answer]').change(function() {
        var url = "{{ route('user.tryouts.saveAnswer',['tryout_id'=>$tryout->id,'question_id'=>$question->id]) }}";
        let answer = this.value;
        $.ajax({
            url: url,
            type: 'POST',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
            },
            data: {user_answer:answer},
            beforeSend: function(e){
                showNotification('alert-info', 'Sedang menyimpan jawaban...', 'top', 'right', null, null);
                $('input[type=radio][name=answer]').prop('disabled',true);
            },
            success: function(data){
                $('input[type=radio][name=answer]').prop('disabled',false);
                if(data.success){
                    showNotification('alert-success', 'Jawaban anda berhasil disimpan', 'top', 'right', null, null);
                } else {
                    showNotification('alert-danger', 'Jawaban anda gagal disimpan, silahkan coba lagi', 'top', 'right', null, null);
                }
            },
            error: function(jqXHR, textStatus, errorThrown){
                showNotification('alert-danger', 'System Error', 'top', 'right', null, null);
                console.log(errorThrown);
            }
        });
    });
</script>
<script>
    function countdownto(target, time, callback) {
        var finish = new Date(time);
        var s = 1000,
            m = s * 60,
            h = m * 60;
            d = h * 24;

        (function timer() {
            var now = new Date();
            var dist = finish - now;

            // var days = Math.floor(dist / d),
            var hours = Math.floor((dist % d) / h),
                minutes = Math.floor((dist % h) / m),
                seconds = Math.floor((dist % m) / s);

            var timestring = hours + 'jam ' + minutes + 'menit ' + seconds + 'detik ';
            
            if (dist > 0) {
                target.innerHTML = timestring
                setTimeout(timer, 1000);
            } else {
                callback()
            }

        })()

    }

    // countdown element
    var countdownel = document.getElementById('countdown');

    // 10 seconds into the future
    var time = new Date();
    time.setSeconds(time.getSeconds() + Number("{{$remaining_time}}"));

    if(Number("{{$remaining_time}}") == 0) {
        @if(!$isTryoutEnd || $result_score == NULL)
        showNotification('alert-warning', 'Waktu Pengerjaan {{$type}} telah habis atau telah selesai', 'bottom', 'left', null, null);
        @endif

        //disabled answer
        $('input[type=radio][name=answer]').prop('disabled',true);
        $('#endBtn').prop('disabled',true);
    } else {
        // countdown function call
        countdownto(countdownel, time, function(){
            showNotification('alert-warning', 'Waktu Pengerjaan {{$type}} telah habis atau telah selesai', 'bottom', 'left', null, null)
        });
    }

</script>
@endpush
@endsection