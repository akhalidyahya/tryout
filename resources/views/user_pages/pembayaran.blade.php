@extends('layouts.base')
@section('title','Tryouts')
@push('customCSS')
<style>
    #copy {
        cursor: pointer;
    }
    #bank1 {
        font-size: 1.3em;
        border: none !important;
        outline: none !important;
        background-color: transparent !important;
        font-weight: bolder;
    }

</style>
@endpush
@section('content')
<meta name="csrf-token" content="{{ csrf_token() }}">
<div class="container-fluid">
    <div class="row clearfix">
        <div class="col-lg-12">
            <div class="card">
                <div class="body">
                    <div class="row">
                        <div class="col-md-12 text-center">
                            <label>Selesaikan Pembayaran Segera</label>
                            <p>Status Pembayaran: Menunggu Pembayaran</p>
                        </div>
                    </div>
                    <table class="table ptsi-table">
                        <thead>
                            <tr>
                                <th scope="col">Transfer Bank </th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>
                                    Nomor Rekening <br>
                                    <input type="text" value="280720211354" id="bank1" disabled> <br>
                                    A.n. Yusuf Fatahillah
                                </td>
                                <td>
                                    <div id="copy">
                                        Salin <i class="material-icons">content_copy</i>
                                    </div>
                                </td>
                                
                            </tr>
                            <tr>
                                <td>
                                    Total Pembayaran <br>
                                    <b>Rp 25.000</b>
                                </td>
                                <!-- <td>
                                    Lihat Detail <i class="material-icons">details</i>
                                </td> -->
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@include('pages.part.modal-tryout')
@push('customJS')
<script>
    $('#copy').click(function(){
        let text = document.getElementById('bank1');
        text.select();
        text.setSelectionRange(0, 99999);

        document.execCommand("copy");
        swal('Nomor rekening tersalin!');
    });
</script>
@endpush
@endsection