@extends('layouts.base')
@section('title','Mulai Tryout')
@push('customCSS')
@endpush
@section('content')
<div class="container-fluid">
    <div class="block-header">
        <h2>Mulai Tryout</h2>
    </div>
    <div class="row clearfix">
        <div class="col-lg-12">
            <div class="card">
                <div class="header">
                    <h2><a href="{{route('user.tryouts.index')}}"><i class="fa fa-chevron-left"></i></a> {{$tryout->tryout_name}}</h2>
                </div>
                <div class="body">
                    <div class="row">
                        <div class="col-md-12">
                            <h4>Petunjuk Pengisian</h4>
                            <p>
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Recusandae perferendis voluptatum error cumque similique ex nesciunt saepe id obcaecati cum fugiat ipsum blanditiis assumenda voluptatem quis placeat, ipsam fugit nulla!
                            </p>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-md-6">
                            <strong>TPA</strong>
                            <p>
                                Waktu: 100 Menit, <br>
                                Jumlah Soal: 100, <br>
                                Waktu mulai: {{ !empty($tryout->result->tpa_start_time) ? @\Carbon\Carbon::parse(@$tryout->result->tpa_start_time)->format('d F Y H:i') : '-' }}
                            </p>
                            <button class="btn btn-primary" onclick="startTryout('TPA')">Mulai TPA</button>
                        </div>
                        <div class="col-md-6">
                            <strong>TBI</strong>
                            <p>
                                Waktu: 50 Menit, <br>
                                Jumlah Soal: 50, <br>
                                Waktu mulai: {{ !empty($tryout->result->tbi_start_time) ? @\Carbon\Carbon::parse(@$tryout->result->tbi_start_time)->format('d F Y H:i') : '-' }}
                            </p>
                            <button class="btn btn-primary" onclick="startTryout('TBI')">Mulai TBI</button>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@push('customJS')
<script>
    function startTryout(type) {
        swal({
            title: "Anda yakin?",
            text: `Dengan ini ${type} akan dimulai`,
            icon: "warning",
            buttons: true,
            dangerMode: true,
        }).then((done) => {
            if (done) {
                let url = "{{route('user.tryouts.startTimer',['tryout_id'=>$tryout->id,'type'=>':type'])}}";
                url = url.replace(':type', type);
                $.ajax({
                    url: url,
                    type: 'GET',
                    success: function(data){
                        if(data.success){
                            var urlRedirect = "{{route('user.tryouts.tryout',['tryout_id'=>$tryout->id,'type'=>':type','question_number'=>1])}}";
                            urlRedirect = urlRedirect.replace(':type', type);
                            window.location.href = urlRedirect;
                        } else {
                            swal('Gagal','Silahkan coba lagi atau hubungi CP','warning');
                        }
                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                        swal({
                            title: 'System Error',
                            text: errorThrown,
                            icon: 'error',
                            timer: '3000'
                        });
                    }
                });
            }
        });
    }
</script>
@endpush
@endsection