<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\TryoutController;
use App\Http\Controllers\QuestionController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\ParticipantController;
use App\Http\Controllers\ExcelController;
use App\Http\Controllers\User\PembayaranController as UserPembayaranController;
use App\Http\Controllers\User\TryoutController as UserTryoutController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});

Route::get('/login',[AuthController::class,'loginIndex'])->name('login.index');
Route::get('/register',[AuthController::class,'registerIndex'])->name('register.index');
Route::get('/activation/{user_id}/{token}',[AuthController::class,'activation'])->name('activation');
// Route::get('/sendActivation/{id}',[AuthController::class,'sendActivation'])->name('activation.send');
Route::post('/register',[AuthController::class,'saveData'])->name('register.save');
Route::post('/login',[AuthController::class,'loginProcess'])->name('login.process');
Route::post('/logout',[AuthController::class,'logout'])->name('logout');

Route::group(['middleware' => ['auth']], function () {
    Route::get('/dashboard', [DashboardController::class,'index'])->name('dashboard.index');
    Route::get('/tryoutFiles/{tryout_id}', [TryoutController::class,'tryoutFiles'])->name('tryouts.files');

    Route::group(['middleware' => ['admin']], function () {
        //tryout
        Route::get('/tryouts', [TryoutController::class,'index'])->name('tryouts.index');
        Route::get('/tryouts/getById', [TryoutController::class,'getTryoutById'])->name('tryouts.getById');
        Route::post('/tryouts', [TryoutController::class,'saveTryout'])->name('tryouts.save');
        Route::delete('/tryouts', [TryoutController::class,'deleteTryout'])->name('tryouts.delete');
        Route::get('/tryoutRank/{tryout_id}', [TryoutController::class,'tryoutRank'])->name('tryouts.rank');
        Route::get('/tryouts/rankDataTable/{tryout_id}', [TryoutController::class,'rankDataTable'])->name('tryouts.rankDataTable');
        Route::get('/tryouts/dataTable', [TryoutController::class,'dataTable'])->name('tryouts.dataTable');
        Route::post('/tryoutFile', [TryoutController::class,'uploadFile'])->name('tryouts.uploadFile');
        Route::delete('/tryoutFile/{id}', [TryoutController::class,'deleteFile'])->name('tryouts.deleteFile');
    
        //questions
        Route::get('/questionsDataTable/{type}', [QuestionController::class,'dataTable'])->name('questions.dataTable');
        Route::post('/questions', [QuestionController::class,'saveQuestion'])->name('questions.save');
        Route::get('/questions/getById', [QuestionController::class,'getQuestionById'])->name('questions.getById');
        Route::delete('/questions', [QuestionController::class,'deleteQuestion'])->name('questions.delete');
        Route::get('/questions/{id}', [QuestionController::class,'index'])->name('questions.index');
    
        //peserta
        Route::get('/participants', [ParticipantController::class,'index'])->name('participants.index');
        Route::get('/participants/getById', [ParticipantController::class,'getParticipantById'])->name('participants.getById');
        Route::post('/participants', [ParticipantController::class,'saveParticipant'])->name('participants.save');
        Route::delete('/participants', [ParticipantController::class,'deleteParticipant'])->name('participants.delete');
        Route::post('/participantsChangeStatus/{user_id}', [ParticipantController::class,'changeStatus'])->name('participants.changeStatus');
        Route::get('/participants/dataTable', [ParticipantController::class,'dataTable'])->name('participants.dataTable');

        //calculate manual
        Route::get('/tryoutEnd/{tryout_id}/type/{type}/user/{user_id}', [UserTryoutController::class,'calculateScoreManual'])->name('tryouts.calculateScoreManual');

        //Export to Excel
        Route::get('/export', [ExcelController::class, 'export'])->name('participants.export');
    });

    Route::group(['prefix' => 'user','middleware'=>['user']], function () {
        //pembayaran
        Route::get('/pembayaran', [UserPembayaranController::class,'index'])->name('pembayaran.index');
        Route::post('/pembayaranBukti', [UserPembayaranController::class,'uploadBukti'])->name('pembayaran.bukti');

        Route::get('/tryouts', [UserTryoutController::class,'index'])->name('user.tryouts.index');
        Route::get('/tryoutInfo/{tryout_id}', [UserTryoutController::class,'tryoutInfo'])->name('user.tryouts.info');
        Route::get('/tryoutRank/{tryout_id}', [UserTryoutController::class,'tryoutRank'])->name('user.tryouts.rank');
        Route::get('/tryoutStartTimer/{tryout_id}/type/{type}', [UserTryoutController::class,'startTryoutTimer'])->name('user.tryouts.startTimer');
        Route::get('/tryoutEnd/{tryout_id}/type/{type}', [UserTryoutController::class,'calculateScore'])->name('user.tryouts.calculateScore');
        Route::get('/tryout/{tryout_id}/type/{type}/question/{question_number}', [UserTryoutController::class,'tryout'])->name('user.tryouts.tryout');
        Route::post('/tryouts/{tryout_id}/saveAnswer/{question_id}', [UserTryoutController::class,'saveAnswer'])->name('user.tryouts.saveAnswer');

        Route::get('/tryouts/dataTable', [UserTryoutController::class,'dataTable'])->name('user.tryouts.dataTable');
        Route::get('/tryouts/rankDataTable/{tryout_id}', [UserTryoutController::class,'rankDataTable'])->name('user.tryouts.rankDataTable');
    });
});